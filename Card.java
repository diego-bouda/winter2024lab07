public class Card{
	private int rank;
	private String suit;

	public Card(int rank, String suit){
		this.rank = rank;
		this.suit = suit;
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		String printRank = Integer.toString(rank);
		if(rank == 13)
			printRank = "king";
		if(rank == 12)
			printRank = "queen";
		if(rank == 11)
			printRank = "jack";
		if(rank == 1)
			printRank = "ace";
		return printRank + " of " +  suit;
	}
	
	public double calculateScore(){
		double score = 0;
		double rank = this.rank*1.0;
		
		if(this.suit.equals("hearts"))
			score = 0.4;
		if(this.suit.equals("spades"))
			score = 0.3;
		if(this.suit.equals("diamonds"))
			score = 0.2;
		if(this.suit.equals("clubs"))
			score = 0.1;
		return score + rank;
	}
}