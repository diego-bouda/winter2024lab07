public class SimpleWar{
	public static void main (String[] args){
		int pointPlayerOne = 0;
		int pointPlayerTwo = 0;
		int round = 0;
		
		Deck deck = new Deck();
		deck.shuffle();
		
		while(deck.numOfCards > 0){
			Card card1 = deck.drawTopCard();
			Card card2 = deck.drawTopCard();
			round += 1;
			System.out.println("-----------------------");
			System.out.println("Round: " + round);
			
			System.out.println(card1);
			System.out.println(card1.calculateScore());
			
			System.out.println(card2);
			System.out.println(card2.calculateScore());
			
			System.out.println("Player one points: " + pointPlayerOne);
			System.out.println("Player two points: " + pointPlayerTwo);
			
			if(card1.calculateScore()<card2.calculateScore()){
				pointPlayerTwo += 1;
			}else{
				pointPlayerOne += 1;
			}
			
			System.out.println("Player one points: " + pointPlayerOne);
			System.out.println("Player two points: " + pointPlayerTwo);
		}
		
		if(pointPlayerOne > pointPlayerTwo)
			System.out.println("Player one won!");
		else
			System.out.println("Player two won!");
	}
}